﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace BlobStorageCacheControlConfigurator
{
    public class BlobStorageClient : IDisposable
    {
        private CloudBlobClient _client;
        private bool _disposed = false;

        public CloudBlobClient Client
        {
            get { return _client; }
        }

        public BlobStorageClient(string connectionString)
        {
            try
            {
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(connectionString);
                _client = storageAccount.CreateCloudBlobClient();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    //_client.EndSetServiceProperties();
                }
                _disposed = true;
            }
        }
        public void Dispose()
        {
            Dispose(true);
        }
    }
}
