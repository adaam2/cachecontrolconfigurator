﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace BlobStorageCacheControlConfigurator
{
    public class BlobStorageCacheController
    {
        private CloudBlobClient _client;
        private const string oneYearHeader = "max-age=31536000, public";
        public BlobStorageCacheController(CloudBlobClient client)
        {
            _client = client;
        }

        public void EnumerateAndSet(string containerName, List<string> fileNames)
        {
            CloudBlobContainer container = _client.GetContainerReference(containerName);

            var blobInfos = container.ListBlobs(useFlatBlobListing:true);

            Parallel.ForEach(blobInfos, (blobInfo) =>
            {
                CloudBlob blob = container.GetBlobReference(blobInfo.Uri.ToString());

                blob.FetchAttributes();

                // set the cache-control header on each blob if necessary

                if (fileNames.Contains(blob.Name))
                {
                    if (blob.Properties.CacheControl != oneYearHeader)
                    {
                        blob.Properties.CacheControl = oneYearHeader;
                        blob.SetProperties();
                    }
                }
            });
        }
    }
}
