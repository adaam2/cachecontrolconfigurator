﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlobStorageCacheControlConfigurator;
using System.Configuration;

namespace BlobStorageCacheControlConfigurator.Tests
{
    class Program
    {
        static void Main(string[] args)
        {
            string blobStorageConnectionString =
                @"DefaultEndpointsProtocol=https;AccountName=accoblobstorageus;AccountKey=sAhNFdp445RTLxvndC4KHYoBCv4LNiFOJXuzlnmDtoROeqDByFAJK2okipDIR4+zrTPNfly1ALo7Tjxm3GetbA==;";

            using (BlobStorageClient blobStorageCacheControlClient = new BlobStorageClient(blobStorageConnectionString))
            {
                var cacheController = new BlobStorageCacheController(blobStorageCacheControlClient.Client);

                cacheController.EnumerateAndSet("samplecontainer1", new List<string>()
                {
                    "blob1.txt", "largeblob1.txt"
                });
            }
        }
    }
}
